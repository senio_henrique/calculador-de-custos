<<<<<<< HEAD
CREATE DATABASE ManageMEI;
use managemei;
>>>>>>> 679995f7a89f36be90120e8c336e380f31371807

CREATE TABLE Usuario (
Nome VARCHAR(50),
CNPJ VARCHAR(19) PRIMARY KEY UNIQUE NOT NULL,
Email VARCHAR(30),
Senha VARCHAR(20)
);

CREATE TABLE DespesaFixa (
Nome VARCHAR(50) PRIMARY KEY UNIQUE NOT NULL,
Preco DECIMAL(6),
Usuario_CNPJ VARCHAR(19),
FOREIGN KEY(Usuario_CNPJ) REFERENCES Usuario(CNPJ)
);

CREATE TABLE MaoDeObra (
CodMao INTEGER PRIMARY KEY AUTO_INCREMENT UNIQUE NOT NULL,
HorasTrabalhadasMes NUMERIC(3),
ValorHoraTrabalhada DECIMAL(6),
SalarioDesejado NUMERIC(6),
Usuario_CNPJ VARCHAR(19),
FOREIGN KEY(Usuario_CNPJ) REFERENCES Usuario (CNPJ)
);

CREATE TABLE Insumo (
CodInsumo INTEGER PRIMARY KEY AUTO_INCREMENT UNIQUE NOT NULL,
PrecoU DECIMAL(6),
Nome VARCHAR(50),
<<<<<<< HEAD
QtdDisponivel DECIMAL(4),
Usuario_CNPJ VARCHAR(19),
FOREIGN KEY(Usuario_CNPJ) REFERENCES Usuario (CNPJ)
);
=======
Email VARCHAR(30),
ID_Produto INTEGER
)
>>>>>>> 679995f7a89f36be90120e8c336e380f31371807

CREATE TABLE Produto (
ID_Produto INTEGER PRIMARY KEY UNIQUE NOT NULL,
NomeProduto VARCHAR(30),
ObservacoesGerais VARCHAR(250),
TempoProducaoPeca NUMERIC(3),
CodInsumo INTEGER,
NomeDespesaFixa VARCHAR(10),
CodMao INTEGER,
Usuario_CNPJ VARCHAR(19),
FOREIGN KEY(CodInsumo) REFERENCES Insumo (CodInsumo),
FOREIGN KEY(NomeDespesaFixa) REFERENCES DespesaFixa (Nome),
FOREIGN KEY(CodMao) REFERENCES MaoDeObra (CodMao),
FOREIGN KEY(Usuario_CNPJ) REFERENCES Usuario (CNPJ)
);
/*

create table controlevendas(
CodVenda integer primary key auto_increment,
ID_ProdutoVendido int(11),
QtdProd integer(4),
ValorUnitProd decimal(4, 2),
ValorTotal decimal(4, 2),
FOREIGN KEY(ID_ProdutoVendido) REFERENCES Produto(ID_Produto)
);

*/

/*INSERT INTO livros (titulo, autor, isbn, edicao, editora, anoPublicacao, qtdePaginas, genero, idioma, quantidade) VALUES ('orgulho e preconceito', 'jane austen', 978-8544001820, 'luxo', 'martin claret', 2018, 424, 'romance', 2);*/


INSERT INTO Usuario(Nome, CNPJ, Email, Senha) VALUES('Sindell', '28.365.649/0001-62', 'sindellkelly@hotmail.com', '123');
INSERT INTO Usuario(Nome, CNPJ, Email, Senha) VALUES('Refrigeração Power', '215.059.682/0001-85', 'power.ar@hotmail.com', '321');

INSERT INTO DespesaFixa(Nome, Preco) VALUES('Energia', 250);
INSERT INTO DespesaFixa(Nome, Preco) VALUES('Água', 50);

INSERT INTO Insumo(CodInsumo, PrecoU, Nome,QtdDisponivel) VALUE(1, 18.00, 'Tecido Qualquer', 30);
INSERT INTO Insumo(CodInsumo, PrecoU, Nome,QtdDisponivel) VALUE(2, 36.00, 'Tecido Sintético', 50);

INSERT INTO MaoDeObra(HorasTrabalhadasMes, ValorHoraTrabalhada, SalarioDesejado) VALUES(176, 11.00, 3000);
INSERT INTO MaoDeObra(HorasTrabalhadasMes, ValorHoraTrabalhada, SalarioDesejado) VALUES(160, 18.00, 2500);

SELECT MaoDeObra.CodMao, Insumo.CodInsumo, DespesaFixa.Nome FROM MaoDeObra INNER JOIN Insumo ON MaoDeObra.Usuario_CNPJ = Insumo.Usuario_CNPJ INNER JOIN DespesaFixa ON Insumo.Usuario_CNPJ = DespesaFixa.Usuario_CNPJ;

DROP database manageMei;