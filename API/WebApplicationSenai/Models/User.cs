﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationSenai.Models
{
    public class User
    {
        public string nome { get; set; }
        public string senha { get; set; }

        public string cnpj { get; set; }

        public User(string nome, string senha, string cnpj)
        {
            this.nome = nome;
            this.senha = senha;
            this.cnpj = cnpj;
        }
    }
}