﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApplicationSenai.Util;

namespace WebApplicationSenai.Controllers
{
    public class UserController : ApiController
    {

        private const string connectionString = "Server=us-cdbr-east-04.cleardb.com; " +
                                                "User Id=b7b54fb717f600; " +
                                                "Pwd=03e4f07f; " +
                                                "Database=heroku_d6a5810b6bfa8cb";
        private MySqlConnection con = null;
        private MySqlCommand query = null;
        private HttpResponseMessage response = null;

        private void connection()
        {
            con = new MySqlConnection(connectionString);
            con.Open();
        }

        [Route("api/User/cadastrar")]
        [HttpGet]
        public HttpResponseMessage cadastrarUser(string nome, string senha, string cnpj)
        {

            if(nome.Length == 0 || senha.Length == 0 || cnpj.Length == 0)
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent, "Preencha com os dados necessarios");
            }
            else
            {
                string msgCnpjValidado = Validator.validar(cnpj);


                if (!msgCnpjValidado.Equals("Cnpj valido"))
                {
                    response = Request.CreateResponse(HttpStatusCode.NotAcceptable, msgCnpjValidado);
                }


                try
                {
                    connection();
                    query = new MySqlCommand("INSERT INTO usuario VALUES(@cnpj,@senha,@nome,null,null);", con);
                    query.Parameters.AddWithValue("@nome", nome);
                    query.Parameters.AddWithValue("@senha", senha);
                    query.Parameters.AddWithValue("@cnpj", cnpj);


                    if (query.ExecuteNonQuery() != 0)
                    {
                        response = Request.CreateResponse(HttpStatusCode.OK, "Usuário adicionado com sucesso");
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.InternalServerError, "O usuario nao foi adicionado");
                    }
                }
                catch (Exception e)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Erro " + e.Message);
                }
            }

            return response;
        }

        [Route("api/User/login")]
        [HttpGet]
        public HttpResponseMessage buscarUsuario(string nome, string senha)
        {
            if (nome.Length == 0 || senha.Length == 0 )
            {
                response = Request.CreateResponse(HttpStatusCode.NoContent, "Preencha com os dados necessarios");
            }
            else
            {
                try
                {
                    connection();
                    query = new MySqlCommand("SELECT Nome, Senha FROM usuario WHERE Nome = @nome",con);

                    query.Parameters.AddWithValue("@nome", nome);

                    MySqlDataReader reader = query.ExecuteReader();

                    string senhaEnc = null;

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            senhaEnc = reader.GetString("Senha");
                        }

                        if(senha == senhaEnc)
                        {
                            response = Request.CreateResponse(HttpStatusCode.OK, "Login autorizado");
                        }
                        else
                        {
                            response = Request.CreateResponse(HttpStatusCode.BadRequest, "Senha incorreta");
                        }
                    }
                    else
                    {
                        response = Request.CreateResponse(HttpStatusCode.NotFound, "Usuario nao encontrado");
                    }

                }catch(Exception e)
                {
                    response = Request.CreateResponse(HttpStatusCode.InternalServerError, "Erro " + e.Message);
                }
            }

            return response;
            
        }


    }
}