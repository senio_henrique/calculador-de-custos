package com.managemei.back.banco.maoObra;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.managemei.back.banco.Conexao;
import com.managemei.back.models.MaoObra;

import java.util.ArrayList;
import java.util.List;

public class MaoObraDAO implements MaoObraCRUD {
    private Conexao conexao;
    private SQLiteDatabase database;
    private ContentValues values;

    public MaoObraDAO(Context context) {
        conexao = new Conexao(context);
        database = conexao.getWritableDatabase();
    }

    @Override
    public long cadastrarMaoObra(MaoObra maoObra, String userId) {
        long retornoId = 0;
        values = new ContentValues();
        values.put("horasTrabalhadas", maoObra.getHorasTrabalhadas());
        values.put("valorHoraTrabalhada", maoObra.getValorHoraTrabalhada());
        values.put("salarioDesejado", maoObra.getSalarioDesejado());
        values.put("usuario_cnpj", userId);

        try {
            retornoId = database.insertOrThrow("MaoObra", null, values);
        } catch (SQLException e) {
            Log.d("ERRO_SQL", e.getMessage());
            retornoId = -1;
        }
        return retornoId;
    }

    @Override
    public List<MaoObra> consultarMaoObra(String userId) {
        List<MaoObra> lista = new ArrayList<>();
        Cursor cursor = database.query("MaoObra",new String[]{"_idMaoObra", "horasTrabalhadas", "valorHoraTrabalhada", "salarioDesejado"},"usuario_cnpj = ?", new String[]{userId}, null, null, null);

        while (cursor.moveToNext()) {
            MaoObra maoObra = new MaoObra(cursor.getInt(0), cursor.getDouble(3), cursor.getDouble(1), cursor.getDouble(2));
            lista.add(maoObra);
        }

        return lista;
    }
}
