package com.managemei.back.banco.usuario;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.managemei.back.banco.Conexao;
import com.managemei.back.models.Usuario;

public class UsuarioDAO implements UsuarioCRUD{
    private Conexao conexao;
    private SQLiteDatabase database;
    private ContentValues values;

    public UsuarioDAO(Context context){
        conexao = new Conexao(context);
        database = conexao.getWritableDatabase();
    }


    @Override
    public Usuario login(String email, String senha) {
        Usuario usuario = null;
        Cursor cursor = database.query("Usuario",new String[]{"cnpj","nome","email","senha"}, "email = ? and senha = ?", new String[]{email,senha},null,null,null);

        if(cursor.getCount() != 0){
            while(cursor.moveToNext()){
                usuario = new Usuario(cursor.getString(1),cursor.getString(2),cursor.getString(0),cursor.getString(3));
            }
        }else{
            usuario = null;
        }

        return usuario;

    }

    @Override
    public long cadastro(Usuario usuario) {
        long retornoId = 0;
        values = new ContentValues();
        values.put("cnpj", usuario.getCnpj());
        values.put("nome", usuario.getNomeEmpresa());
        values.put("email", usuario.getEmail());
        values.put("senha", usuario.getSenha());
        try {
            database.insertOrThrow("Usuario", null, values);
            Log.d("ERRO_SQL", "Adicionado com Sucesso!!!");

        } catch (SQLException e) {
            Log.d("ERRO_SQL", e.getMessage());
        }
        return retornoId;
    }
}
