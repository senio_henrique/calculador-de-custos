package com.managemei.back.banco.usuario;

import com.managemei.back.models.Usuario;

public interface UsuarioCRUD {

    Usuario login(String email, String senha);
    long cadastro(Usuario usuario);

}
