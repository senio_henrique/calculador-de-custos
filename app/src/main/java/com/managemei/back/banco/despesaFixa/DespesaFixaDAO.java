package com.managemei.back.banco.despesaFixa;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.managemei.back.banco.Conexao;
import com.managemei.back.models.DespesaFixa;

import java.util.ArrayList;
import java.util.List;

public class DespesaFixaDAO implements DespesaFixaCRUD {
    private Conexao conexao;
    private SQLiteDatabase database;
    private ContentValues values;

    public DespesaFixaDAO(Context context) {
        conexao = new Conexao(context);
        database = conexao.getWritableDatabase();
    }

    @Override
    public long cadastrarDespesas(DespesaFixa despesaFixa, String userId) {
        long retornoId = 0;
        values = new ContentValues();
        values.put("nomeDespesa", despesaFixa.getNomeDespesa());
        values.put("precoDespesa", despesaFixa.getPrecoDespesa());
        values.put("usuario_cnpj", userId);
        try {
            retornoId = database.insertOrThrow("DespesasFixas", null, values);
            Log.d("ERRO_SQL", "Adicionado com Sucesso!!!");

        } catch (SQLException e) {
            retornoId = -1;
            Log.d("ERRO_SQL", e.getMessage());
        }
        return retornoId;
    }


    @Override
    public List<DespesaFixa> consultarDespesas(String userId) {
        List<DespesaFixa> list = new ArrayList<>();
        Cursor cursor = database.query("DespesasFixas",new String[]{"nomeDespesa","precoDespesa"}, "usuario_cnpj = ?", new String[]{userId},null,null,null);

        while (cursor.moveToNext()){
            DespesaFixa despesaFixa = new DespesaFixa(cursor.getString(0),cursor.getDouble(1));
            list.add(despesaFixa);
        }
        return list;
    }
}
