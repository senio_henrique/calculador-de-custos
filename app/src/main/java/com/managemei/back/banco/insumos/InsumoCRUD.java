package com.managemei.back.banco.insumos;

import com.managemei.back.models.Insumo;

import java.util.List;

public interface InsumoCRUD {
    long cadastrarInsumo(Insumo insumo, String userId);
    List<Insumo> consultarInsumos(String userId);
}
