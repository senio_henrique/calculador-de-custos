package com.managemei.back.banco.despesaFixa;

import com.managemei.back.models.DespesaFixa;

import java.util.List;

public interface DespesaFixaCRUD {
    long cadastrarDespesas(DespesaFixa despesaFixa, String userId);

    List<DespesaFixa> consultarDespesas(String userId);
}
