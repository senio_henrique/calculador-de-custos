package com.managemei.back.banco.produto;

import com.managemei.back.models.Produto;

import java.util.List;

public interface ProdutoCRUD {
    void getKeys(String userId);
    long cadastrarProduto(Produto produto);
    List<Produto> consultarProduto(String userId);

}
