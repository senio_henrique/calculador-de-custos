package com.managemei.back.banco.insumos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.managemei.back.banco.Conexao;
import com.managemei.back.models.Insumo;

import java.util.ArrayList;
import java.util.List;

public class InsumoDAO implements InsumoCRUD{
    private Conexao conexao;
    private ContentValues values;
    private SQLiteDatabase database;

    public InsumoDAO(Context context) {
        conexao = new Conexao(context);
        database = conexao.getWritableDatabase();
    }

    @Override
    public long cadastrarInsumo(Insumo insumo, String userId) {
        long retornoId = 0;
        values = new ContentValues();
        values.put("nome", insumo.getNome());
        values.put("preco_u", insumo.getPreco_u());
        values.put("qtdDisponivel", insumo.getQtdDisponivel());
        values.put("usuario_cnpj", userId);
        try {
            retornoId = database.insertOrThrow("Insumo", null, values);

        } catch (SQLException e) {
            Log.d("ERRO_SQL", e.getMessage());
            retornoId = -1;
        }
        return retornoId;
    }

    @Override
    public List<Insumo> consultarInsumos(String userId) {
        List<Insumo> list = new ArrayList<>();
        Cursor cursor = database.query("Insumo",new String[]{"codInsumo","nome","preco_U","qtdDisponivel"}, "usuario_cnpj = ?",new String[]{userId},null,null,null);

        while (cursor.moveToNext()){
            Insumo insumo = new Insumo(cursor.getInt(0),cursor.getDouble(2),cursor.getDouble(3),cursor.getString(1));
            list.add(insumo);
        }
        return list;
    }
}
