package com.managemei.back.banco;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class Conexao extends SQLiteOpenHelper {
    private static final String NAME = "bancoManageMEI.db";
    private static final int VERSION = 1;

    public Conexao(@Nullable Context context) {
        super(context, NAME, null, VERSION);
    }

    //25.45
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Usuario(cnpj text unique primary key not null, nome text, email text unique, senha text);");

        db.execSQL("create table MaoObra(_idMaoObra integer primary key autoincrement unique, horasTrabalhadas text, valorHoraTrabalhada real, salarioDesejado real, usuario_cnpj text, foreign key(usuario_cnpj) references Usuario(cnpj));");

        db.execSQL("create table DespesasFixas(nomeDespesa text unique primary key unique, precoDespesa real, usuario_cnpj text, foreign key(usuario_cnpj) references Usuario(cnpj));");

        db.execSQL("create table Insumo(codInsumo integer primary key autoincrement unique, nome text, preco_U real, qtdDisponivel real, usuario_cnpj text, foreign key(usuario_cnpj) references Usuario(cnpj));");

        db.execSQL("create table Produto(idProduto integer primary key autoincrement unique, nome text, porcentagemLucro real, custoProducao real, precoVenda real, observacoesGerais text, codInsumo integer, nomeDespesaFixa text, codMao integer, usuario_cnpj text, foreign key(codInsumo) references Insumo(codInsumo), foreign key(nomeDespesaFixa) references DespesaFixa(nome), foreign key(codMao) references MaoObra(codMao), foreign key(usuario_cnpj) references Usuario(cnpj));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
