package com.managemei.back.banco.maoObra;

import com.managemei.back.models.MaoObra;

import java.util.List;

public interface MaoObraCRUD {
    long cadastrarMaoObra(MaoObra maoObra, String userId);
    List<MaoObra> consultarMaoObra(String userId);
}
