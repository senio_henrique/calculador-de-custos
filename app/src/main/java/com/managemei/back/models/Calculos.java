package com.managemei.back.models;

public abstract class Calculos {
    private float valorEmbalagem, precoMaterial, qtdUtilizada, salario, tempoProducao, horasMes;
    private float valorInsumo, valorMaoObra, valorDespesasFixas, valorCustoPeca, valorConfeccao, valorListaInsumos, somaListaDespesas;

    private float calculoInsumo(float precoMaterial, float qtdUtilizada) {
        return valorInsumo =  precoMaterial * qtdUtilizada;
    }

    private float calculoMaoObra(float salario, float tempoProducao, float horasMes) {
        return valorMaoObra = salario/(horasMes*60)*tempoProducao;
    }

    private float despesasFixas(float horasMes, float tempoProducao) {
        return valorDespesasFixas = somaListaDespesas/(horasMes/(tempoProducao/60));
    }

    private float confeccao() {
        return valorConfeccao = valorMaoObra + valorDespesasFixas;
    }

    private float custoPeca(float valorEmbalagem) {
        return valorCustoPeca = valorEmbalagem + valorListaInsumos + valorMaoObra + valorDespesasFixas;
    }
}
