package com.managemei.back.models;


import java.io.Serializable;

public class Usuario implements Serializable {
    private String nomeEmpresa, email, cnpj, senha;

    public Usuario(String nomeEmpresa, String email, String cnpj, String senha) {
        this.nomeEmpresa = nomeEmpresa;
        this.email = email;
        this.cnpj = cnpj;
        this.senha = senha;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public String getEmail() {
        return email;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getSenha() {
        return senha;
    }
}
