package com.managemei.back.models;

import java.io.Serializable;

public class DespesaFixa implements Serializable {
    private String nomeDespesa;
    private double precoDespesa = 0;

    public DespesaFixa(String nomeDespesa, double precoDespesa) {
        this.nomeDespesa = nomeDespesa;
        this.precoDespesa = precoDespesa;
    }

    public String getNomeDespesa() {
        return nomeDespesa;
    }

    public double getPrecoDespesa() {
        return precoDespesa;
    }
}
