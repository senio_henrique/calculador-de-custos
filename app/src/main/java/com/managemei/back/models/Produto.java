package com.managemei.back.models;

import java.io.Serializable;

public class Produto implements Serializable {
    private Integer _id;
    private String nome, observacoesGerais, tempoProducao;
    private double lucro, custoProducao, precoVenda;

    public Produto(Integer _id, String nome, String observacoesGerais, String tempoProducao, double lucro, double custoProducao, double precoVenda) {
        this._id = _id;
        this.nome = nome;
        this.observacoesGerais = observacoesGerais;
        this.tempoProducao = tempoProducao;
        this.lucro = lucro;
        this.custoProducao = custoProducao;
        this.precoVenda = precoVenda;
    }

    public Produto(String nome, String observacoesGerais, String tempoProducao, double lucro, double custoProducao, double precoVenda) {
        this.nome = nome;
        this.observacoesGerais = observacoesGerais;
        this.tempoProducao = tempoProducao;
        this.lucro = lucro;
        this.custoProducao = custoProducao;
        this.precoVenda = precoVenda;
    }

    public Integer get_id() {
        return _id;
    }

    public String getNome() {
        return nome;
    }

    public String getObservacoesGerais() {
        return observacoesGerais;
    }

    public String getTempoProducao() {
        return tempoProducao;
    }

    public double getLucro() {
        return lucro;
    }

    public double getCustoProducao() {
        return custoProducao;
    }

    public double getPrecoVenda() {
        return precoVenda;
    }
}
