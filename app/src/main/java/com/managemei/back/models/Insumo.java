package com.managemei.back.models;

import java.io.Serializable;

public class Insumo implements Serializable {
    private Integer _id;
    private double preco_u, qtdDisponivel;
    private String nome;

    public Insumo(Integer _id, double preco_u, double qtdDisponivel, String nome) {
        this._id = _id;
        this.preco_u = preco_u;
        this.qtdDisponivel = qtdDisponivel;
        this.nome = nome;
    }

    public Insumo(double preco_u, double qtdDisponivel, String nome) {
        this.preco_u = preco_u;
        this.qtdDisponivel = qtdDisponivel;
        this.nome = nome;
    }

    public Integer get_id() {
        return _id;
    }

    public double getPreco_u() {
        return preco_u;
    }

    public double getQtdDisponivel() {
        return qtdDisponivel;
    }

    public String getNome() {
        return nome;
    }

}
