package com.managemei.back.models;

import java.io.Serializable;

public class MaoObra implements Serializable {
    private Integer _id;
    private Double salarioDesejado, horasTrabalhadas, valorHoraTrabalhada;

    public MaoObra(Integer _id, Double salarioDesejado, Double horasTrabalhadas, Double valorHoraTrabalhada) {
        this._id = _id;
        this.salarioDesejado = salarioDesejado;
        this.horasTrabalhadas = horasTrabalhadas;
        this.valorHoraTrabalhada = valorHoraTrabalhada;
    }

    public MaoObra(Double salarioDesejado, Double horasTrabalhadas, Double valorHoraTrabalhada) {
        this.salarioDesejado = salarioDesejado;
        this.horasTrabalhadas = horasTrabalhadas;
        this.valorHoraTrabalhada = valorHoraTrabalhada;
    }

    public Integer get_id() {
        return _id;
    }

    public Double getSalarioDesejado() {
        return salarioDesejado;
    }

    public Double getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public Double getValorHoraTrabalhada() {
        return valorHoraTrabalhada;
    }

}
