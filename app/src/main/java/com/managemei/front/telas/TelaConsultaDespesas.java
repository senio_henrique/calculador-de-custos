package com.managemei.front.telas;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.banco.despesaFixa.DespesaFixaDAO;
import com.managemei.back.models.DespesaFixa;
import com.managemei.back.models.Usuario;
import com.managemei.front.adapters.DespesaFixaAdapter;

import java.util.ArrayList;
import java.util.List;

public class TelaConsultaDespesas extends AppCompatActivity {

    private ListView listViewConsultarDespesas;
    private List<DespesaFixa> list = new ArrayList<>();
    private DespesaFixaDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_consulta_despesas);
        getSupportActionBar().hide();

        listViewConsultarDespesas = findViewById(R.id.listViewConsultarDespesas);

        Bundle extras = getIntent().getExtras();
        Usuario usuario = (Usuario) extras.getSerializable("userLogado");

        dao = new DespesaFixaDAO(this);
        list = dao.consultarDespesas(usuario.getCnpj());

        if(list.size() != 0){
            final DespesaFixaAdapter adapter = new DespesaFixaAdapter(list,this);
            listViewConsultarDespesas.setAdapter(adapter);
        }


    }

    public void onBackPressed(View view) {
        this.finish();
    }
}