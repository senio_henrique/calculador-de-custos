package com.managemei.front.telas;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.banco.despesaFixa.DespesaFixaDAO;
import com.managemei.back.models.DespesaFixa;
import com.managemei.back.models.Usuario;

public class TelaCadastroDespesasFixas extends AppCompatActivity {
    private EditText editTextNomeDesp, editTextPrecoDesp;
    private Button buttonCadastrarDesp;
    private Usuario usuario;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cadastro_despesas_fixas);
        getSupportActionBar().hide();

        editTextNomeDesp = findViewById(R.id.editTextNomeDesp);
        editTextPrecoDesp = findViewById(R.id.editTextPrecoDesp);
        buttonCadastrarDesp = findViewById(R.id.buttonCadastrarDesp);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("userLogado");


        buttonCadastrarDesp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nomeDesp;
                String precoDesp;

                nomeDesp = editTextNomeDesp.getText().toString();
                precoDesp = editTextPrecoDesp.getText().toString();

                if (!(nomeDesp.isEmpty() && precoDesp.isEmpty())) {
                    DespesaFixa despesaFixa = new DespesaFixa(nomeDesp,Double.parseDouble(precoDesp));
                    DespesaFixaDAO dao = new DespesaFixaDAO(getApplicationContext());

                    if(dao.cadastrarDespesas(despesaFixa,usuario.getCnpj()) != -1){
                        finish();
                    }else{
                        Log.d("ERRO_SQL", "erro");
                    }
                } else {
                    editTextNomeDesp.setText("");
                    editTextNomeDesp.setError("Insira todos os dados");
                }
            }
        });
    }

    public void onBackPressed(View view){
        this.finish();
    }
}