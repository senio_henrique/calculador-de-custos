    package com.managemei.front.telas;

    import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
    import com.managemei.back.banco.usuario.UsuarioDAO;
    import com.managemei.back.models.Usuario;


    public class MainActivity extends AppCompatActivity {

        private EditText editTextEmailLogin, editTextSenhaLogin;
        private Button buttonLogin, buttonRedirectCadastrar;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            getWindow().requestFeature(Window.FEATURE_ACTIVITY_TRANSITIONS);
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            getSupportActionBar().hide();

            editTextEmailLogin = findViewById(R.id.editTextEmailCadastroUser);
            editTextSenhaLogin = findViewById(R.id.editTextSenhaCadastroUser);
            buttonLogin = findViewById(R.id.buttonCadastroUser);
            buttonRedirectCadastrar = findViewById(R.id.buttonRedirectCadastrar);

            buttonLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String email = null, senha = null;
                    email = editTextEmailLogin.getText().toString();
                    senha = editTextSenhaLogin.getText().toString();

                    if(email.isEmpty()){
                        editTextEmailLogin.setError("Preencha o email");
                    }if(senha.isEmpty()){
                        editTextSenhaLogin.setError("Preencha a senha");
                    }
                    else{
                        if(email.contains("@") && email.contains(".com")){
                            UsuarioDAO dao = new UsuarioDAO(getApplicationContext());
                            Usuario usuario;

                            usuario = dao.login(email,senha);

                            if(usuario != null){
                                Toast.makeText(MainActivity.this, "Login realizado com sucesso", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), TelaInicial.class);
                                intent.putExtra("userLogado", usuario);
                                startActivity(intent);
                            }else{
                                editTextEmailLogin.setText("");
                                editTextSenhaLogin.setText("");
                                Toast.makeText(MainActivity.this, "Dados incorretos", Toast.LENGTH_LONG).show();
                            }



                        }else{
                            editTextEmailLogin.setError("Email inválido");
                        }
                    }
                }
            });

            buttonRedirectCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), TelaCadastroUsuario.class);
                    startActivity(intent);
                }
            });
        }
    }