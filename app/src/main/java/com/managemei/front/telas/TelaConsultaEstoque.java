package com.managemei.front.telas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.managemei.R;

public class TelaConsultaEstoque extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_consulta_estoque);
        getSupportActionBar().hide();
    }

    public void onBackPressed(View view) {
        this.finish();
    }
}