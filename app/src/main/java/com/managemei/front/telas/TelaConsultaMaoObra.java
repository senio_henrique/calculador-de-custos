package com.managemei.front.telas;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.banco.maoObra.MaoObraDAO;
import com.managemei.back.models.MaoObra;
import com.managemei.back.models.Usuario;
import com.managemei.front.adapters.MaoObraAdapter;

import java.util.ArrayList;
import java.util.List;

public class TelaConsultaMaoObra extends AppCompatActivity {
    private ListView listViewMaoObra;
    private List<MaoObra> list = new ArrayList<>();
    private MaoObraDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_consulta_mao_obra);
        getSupportActionBar().hide();

        Bundle extras = getIntent().getExtras();
        Usuario usuario = (Usuario) extras.getSerializable("userLogado");

        listViewMaoObra = findViewById(R.id.listViewMaoObra);

        dao = new MaoObraDAO(this);
        list = dao.consultarMaoObra(usuario.getCnpj());

        if(list.size() != 0){
            final MaoObraAdapter adapter = new MaoObraAdapter(list,this);
            listViewMaoObra.setAdapter(adapter);
        }
    }

    public void onBackPressed(View view) {
        this.finish();
    }
}