package com.managemei.front.telas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.models.Usuario;

public class TelaMenuConsultar extends AppCompatActivity {

    private Button buttonConsultarEstoque, buttonConsultarDespesas, buttonConsultarInsumos, buttonMaoObra;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_menu_consultar);
        getSupportActionBar().hide();

        buttonConsultarEstoque = findViewById(R.id.buttonConsultarEstoque);
        buttonConsultarDespesas = findViewById(R.id.buttonConsultarDespesas);
        buttonConsultarInsumos = findViewById(R.id.buttonConsultarInsumos);
        buttonMaoObra = findViewById(R.id.buttonMaoObra);


        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("userLogado");


    }


    public void onPressSearch(View view){
        if(view == buttonConsultarEstoque){
            initActivity(TelaConsultaEstoque.class);
        }else if(view == buttonConsultarDespesas){
            initActivity(TelaConsultaDespesas.class);
        }else if(view == buttonConsultarInsumos){
            initActivity(TelaConsultaInsumos.class);
        }else if(view == buttonMaoObra){
            initActivity(TelaConsultaMaoObra.class);
        }

    }


    public void initActivity(Class<?> classe){
        Intent intent = new Intent(getApplicationContext(), classe);
        intent.putExtra("userLogado",usuario);
        startActivity(intent);
    }

    public void onBackPressed(View view){
        this.finish();
    }
}