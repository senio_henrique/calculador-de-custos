package com.managemei.front.telas;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.banco.insumos.InsumoDAO;
import com.managemei.back.models.Insumo;
import com.managemei.back.models.Usuario;

public class TelaCadastroInsumos extends AppCompatActivity {

    private EditText editTextCadastroInsumosNome, editTextCadastroInsumosPrecoU, editTextCadastroInsumosQuantidade;
    private Button buttonCadastrarInsumo;
    private Usuario usuario;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cadastro_insumos);
        getSupportActionBar().hide();

        editTextCadastroInsumosNome = findViewById(R.id.editTextCadastroInsumosNome);
        editTextCadastroInsumosPrecoU = findViewById(R.id.editTextCadastroInsumosPrecoU);
        editTextCadastroInsumosQuantidade = findViewById(R.id.editTextCadastroInsumosQuantidade);
        buttonCadastrarInsumo = findViewById(R.id.buttonCadastrarInsumo);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("userLogado");

        buttonCadastrarInsumo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome, preco, qtd;
                Insumo insumo;
                InsumoDAO dao;

                nome = editTextCadastroInsumosNome.getText().toString();
                preco = editTextCadastroInsumosPrecoU.getText().toString();
                qtd = editTextCadastroInsumosQuantidade.getText().toString();

                if(nome != null && preco != null && qtd != null){
                    insumo = new Insumo(Double.parseDouble(preco),Double.parseDouble(qtd),nome);
                    dao = new InsumoDAO(getApplicationContext());

                    if(dao.cadastrarInsumo(insumo,usuario.getCnpj()) != -1){
                        finish();
                    }else{
                        Log.d("ERRO_SQL","ERRO");
                    }
                }
            }
        });
    }

    public void onBackPressed(View view) {
        this.finish();
    }
}