package com.managemei.front.telas;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.banco.insumos.InsumoDAO;
import com.managemei.back.models.Insumo;
import com.managemei.back.models.Usuario;
import com.managemei.front.adapters.InsumoAdapter;

import java.util.ArrayList;
import java.util.List;

public class TelaConsultaInsumos extends AppCompatActivity {
    private ListView listViewConsultaInsumos;
    private List<Insumo> list = new ArrayList<>();
    private InsumoDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_consulta_insumos);
        getSupportActionBar().hide();

        listViewConsultaInsumos = findViewById(R.id.listViewConsultaInsumos);

        Bundle extras = getIntent().getExtras();
        Usuario usuario = (Usuario) extras.getSerializable("userLogado");

        dao = new InsumoDAO(this);
        list = dao.consultarInsumos(usuario.getCnpj());

        if(list.size() != 0){
            final InsumoAdapter adapter = new InsumoAdapter(list,this);
            listViewConsultaInsumos.setAdapter(adapter);
        }


    }

    public void onBackPressed(View view) {
        this.finish();
    }
}