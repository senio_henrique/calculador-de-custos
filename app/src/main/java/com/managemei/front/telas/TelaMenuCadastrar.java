package com.managemei.front.telas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.models.Usuario;

public class TelaMenuCadastrar extends AppCompatActivity {

    private Button buttonCadastrarProdutos, buttonCadastrarDespesas, buttonCadastrarMaoObra, buttonCadastrarInsumos;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_menu_cadastrar);
        getSupportActionBar().hide();

        buttonCadastrarProdutos = findViewById(R.id.buttonCadastrarProdutos);
        buttonCadastrarDespesas = findViewById(R.id.buttonCadastrarDespesas);
        buttonCadastrarMaoObra = findViewById(R.id.buttonCadastrarMaoObra);
        buttonCadastrarInsumos = findViewById(R.id.buttonCadastrarInsumos);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("userLogado");
    }

    public void initActivity(Class<?> classe){
        Intent intent = new Intent(getApplicationContext(), classe);
        intent.putExtra("userLogado",usuario);
        startActivity(intent);
    }

    public void abrirTelaCadastro(View view){
        if(view == buttonCadastrarProdutos){
            initActivity(TelaCadastroProduto.class);
        }else if(view == buttonCadastrarDespesas){
            initActivity(TelaCadastroDespesasFixas.class);
        }else if(view == buttonCadastrarMaoObra){
            initActivity(TelaCadastroMaoDeObra.class);
        }else if(view == buttonCadastrarInsumos){
            initActivity(TelaCadastroInsumos.class);
        }

    }

    public void onBackPressed(View view){
        this.finish();
    }
}