package com.managemei.front.telas;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.models.Usuario;

public class TelaInicial extends AppCompatActivity {

    ImageButton btnConsultar, btnAdicionar;
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_inicial);
        getSupportActionBar().hide();

        btnConsultar = findViewById(R.id.buttonConsultar);
        btnAdicionar = findViewById(R.id.buttonAdicionar);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("userLogado");


        btnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TelaMenuConsultar.class);
                intent.putExtra("userLogado",usuario);
                startActivity(intent);
            }
        });

        btnAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TelaMenuCadastrar.class);
                intent.putExtra("userLogado",usuario);
                startActivity(intent);
            }
        });
    }

    public void gerarToken(View view){
        Intent intent = new Intent(getApplicationContext(), TelaToken.class);
        startActivity(intent);
    }
}