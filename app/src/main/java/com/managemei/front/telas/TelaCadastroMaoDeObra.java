package com.managemei.front.telas;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.banco.maoObra.MaoObraDAO;
import com.managemei.back.models.MaoObra;
import com.managemei.back.models.Usuario;

public class TelaCadastroMaoDeObra extends AppCompatActivity {
    private EditText editTextSalarioDesejado, editTextQtdHoras;
    private TextView textViewValorHoraTrabalhada;
    private Button buttonTelaCadastrarMaoObra;
    private Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cadastro_mao_de_obra);
        getSupportActionBar().hide();

        editTextSalarioDesejado = findViewById(R.id.editTextSalarioDesejado);
        editTextQtdHoras = findViewById(R.id.editTextQtdHoras);
        buttonTelaCadastrarMaoObra = findViewById(R.id.buttonTelaCadastrarMaoObra);

        Bundle extras = getIntent().getExtras();
        usuario = (Usuario) extras.getSerializable("userLogado");

        buttonTelaCadastrarMaoObra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String salario, qtdHoras, valorHora;

                salario = editTextSalarioDesejado.getText().toString();
                qtdHoras = editTextQtdHoras.getText().toString();
                valorHora = String.valueOf(Math.round(Double.parseDouble(salario)/Double.parseDouble(qtdHoras)));
                MaoObra maoObra = new MaoObra(Double.parseDouble(salario), Double.parseDouble(qtdHoras), Double.parseDouble(valorHora));
                MaoObraDAO dao = new MaoObraDAO(getApplicationContext());

                /**VALIDAR NULL**/

                if (dao.cadastrarMaoObra(maoObra, usuario.getCnpj()) != -1) {
                    finish();
                } else {
                    Log.d("ERRO_SQL", "Erro");
                }
            }
        });
    }

    public void onBackPressed(View view){
        this.finish();
    }
}