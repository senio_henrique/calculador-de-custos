package com.managemei.front.telas;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.managemei.R;
import com.managemei.back.banco.usuario.UsuarioDAO;
import com.managemei.back.models.Usuario;
import com.managemei.util.Validator;

public class TelaCadastroUsuario extends AppCompatActivity {

    private EditText editTextNomeCadastroUser, editTextCnpjCadastroUser, editTextEmailCadastroUser, editTextSenhaCadastroUser;
    private Button buttonCadastroUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cadastro_usuario);
        getSupportActionBar().hide();

        editTextNomeCadastroUser = findViewById(R.id.editTextNomeCadastroUser);
        editTextCnpjCadastroUser = findViewById(R.id.editTextCnpjCadastroUser);
        editTextEmailCadastroUser = findViewById(R.id.editTextEmailCadastroUser);
        editTextSenhaCadastroUser = findViewById(R.id.editTextSenhaCadastroUser);
        buttonCadastroUser = findViewById(R.id.buttonCadastroUser);


        buttonCadastroUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nome, cnpj, email, senha;
                nome = editTextNomeCadastroUser.getText().toString();
                cnpj = editTextCnpjCadastroUser.getText().toString();
                email = editTextEmailCadastroUser.getText().toString();
                senha = editTextSenhaCadastroUser.getText().toString();


                if (Validator.isCNPJ(cnpj)) {
                    if (!(cnpj.contains(".") && cnpj.contains("/") && cnpj.contains("-"))) {
                        cnpj = Validator.imprimeCNPJ(cnpj);
                    }

                    Usuario usuario = new Usuario(nome, email, cnpj, senha);

                    UsuarioDAO dao = new UsuarioDAO(getApplicationContext());

                    if(dao.cadastro(usuario) != -1){
                        finish();
                    }


                    } else {
                    editTextCnpjCadastroUser.setText("");
                    editTextCnpjCadastroUser.setError("Cnpj inválido");
                    }

            }
        });
    }

    public void onBackPressed(View view) {
        this.finish();
    }
}