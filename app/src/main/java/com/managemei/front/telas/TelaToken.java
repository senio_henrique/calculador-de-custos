package com.managemei.front.telas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.managemei.R;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class TelaToken extends AppCompatActivity {
    TextView textViewToken,textViewTimerToken;
    ProgressBar progressBarToken;
    Timer timer = null;
    int i = 60;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_token);
        getSupportActionBar().hide();

        textViewToken = findViewById(R.id.textViewToken);
        textViewTimerToken = findViewById(R.id.textViewTimerToken);
        progressBarToken = findViewById(R.id.progressBarToken);

        if(timer == null){
            timer = new Timer();

            TimerTask tarefa = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textViewToken.setText(String.valueOf(geraToken()));
                        }
                    });
                }
            };

            TimerTask tarefaCronometro = new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Log.d("TIMER_BAR","Executou " + i);
                            progressBarToken.setProgress(i*100/60);

                            if(i == 0){
                                i = 60;
                            }



                            i--;
                        }
                    });
                }
            };

            timer.schedule(tarefaCronometro,0,1000);
            timer.schedule(tarefa, 0, 60000);
        }
    }

    public StringBuilder geraToken(){
        String previousToken = "";
        Random random = new Random();
        StringBuilder token = new StringBuilder("");

        if(previousToken.equals("")){
            for(int i = 0; i < 6; i++){
                token.append(random.nextInt(9));
            }
        }else{
            if(String.valueOf(token).equals(previousToken)){
                for(int i = 0; i < 6; i++){
                    token.append(random.nextInt(9));
                }
            }
        }
        return token;
    }



    public void onBackPressed (View view) {
        this.finish();
    }
}