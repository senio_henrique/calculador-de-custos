package com.managemei.front.telas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.managemei.R;

public class TelaCadastroProduto extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_cadastro_de_produto);
        getSupportActionBar().hide();
    }

    public void onBackPressed(View view){
        this.finish();
    }
}