package com.managemei.front.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.managemei.R;
import com.managemei.back.models.Insumo;

import java.util.List;

public class InsumoAdapter extends BaseAdapter {
    private List<Insumo> list;
    private Activity activity;

    public InsumoAdapter(List<Insumo> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return list.get(i).get_id();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View v = activity.getLayoutInflater().inflate(R.layout.item_tela_consultar_insumos,parent,false);
        TextView textViewValorModeloInsumos = v.findViewById(R.id.textViewValorModeloInsumos);
        TextView textViewQtdModeloInsumos = v.findViewById(R.id.textViewQtdModeloInsumos);
        TextView textViewNomeModeloInsumo = v.findViewById(R.id.textViewNomeModeloInsumo);

        Insumo insumo = list.get(i);

        textViewNomeModeloInsumo.setText(insumo.getNome());
        textViewValorModeloInsumos.setText(String.valueOf(insumo.getPreco_u()));
        textViewQtdModeloInsumos.setText(String.valueOf(insumo.getQtdDisponivel()));

        return v;
    }
}
