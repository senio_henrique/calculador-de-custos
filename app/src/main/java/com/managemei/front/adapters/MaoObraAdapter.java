package com.managemei.front.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.managemei.R;
import com.managemei.back.models.MaoObra;

import java.util.List;

public class MaoObraAdapter extends BaseAdapter {
    private List<MaoObra> maoObraList;
    private Activity activity;

    public MaoObraAdapter(List<MaoObra> maoObraList, Activity activity) {
        this.maoObraList = maoObraList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return maoObraList.size();
    }

    @Override
    public Object getItem(int i) {
        return maoObraList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return maoObraList.get(i).get_id();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View v = activity.getLayoutInflater().inflate(R.layout.item_tela_consulta_mao_obra,parent,false);
        TextView textViewSalarioModeloMaoObra = v.findViewById(R.id.textViewSalarioModeloMaoObra);
        TextView textViewHorasMesModeloMaoObra = v.findViewById(R.id.textViewHorasMesModeloMaoObra);
        TextView textViewValorModeloMaoObra = v.findViewById(R.id.textViewValorModeloMaoObra);

        MaoObra maoObra = maoObraList.get(i);
        textViewSalarioModeloMaoObra.setText(maoObra.getSalarioDesejado().toString());
        textViewHorasMesModeloMaoObra.setText(maoObra.getHorasTrabalhadas().toString());
        textViewValorModeloMaoObra.setText(maoObra.getValorHoraTrabalhada().toString());

        return v;
    }
}
