package com.managemei.front.adapters;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.managemei.R;
import com.managemei.back.models.DespesaFixa;

import java.util.List;

public class DespesaFixaAdapter extends BaseAdapter {
    private List<DespesaFixa> list;
    private Activity activity;

    public DespesaFixaAdapter(List<DespesaFixa> list, Activity activity) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View v = activity.getLayoutInflater().inflate(R.layout.item_tela_consultar_despesas, parent, false);
        TextView textViewNomeModeloDespesa = v.findViewById(R.id.textViewNomeModeloDespesa);
        TextView textViewValorModeloDespesa = v.findViewById(R.id.textViewValorModeloDespesa);

        DespesaFixa despesa = list.get(i);
        textViewNomeModeloDespesa.setText(despesa.getNomeDespesa());
        textViewValorModeloDespesa.setText(String.valueOf(despesa.getPrecoDespesa()));
        return v;
    }
}
